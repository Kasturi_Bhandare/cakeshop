const items=[
    {
        "id":1,
        "name":"Chocolate Curls",
        "price":45,
    },
    {
        "id":2,
        "name":"Spun Sugar",
        "price":35,
    },
    {
        "id":3,
        "name":"Cadbury",
        "price":50,
    },
    {
        "id":4,
        "name":"Royal Icing",
        "price":70,
    },
    {
        "id":5,
        "name":"Sprinkles",
        "price":30,
    },
    {
        "id":6,
        "name":"Ferrero Rocher",
        "price":80,
    },
    {
        "id":7,
        "name":"Chocolate Galore",
        "price":65,
    },
    {
        "id":8,
        "name":"Choco Wafer sticks",
        "price":55,
    },
    {
        "id":9,
        "name":"Fruit Topping(seasonal)",
        "price":100,
    },
    {
        "id":10,
        "name":"Edible flowers",
        "price":60,
    },

]
var TotalAmount=0;
var ItemCount=0
function AddToCart(id)
{
    var itemid=id;
    var item=items.find(item => item.id ===itemid);
    console.log(item);
    var itemcost=item.price;
    var itemname=item.name;
    ItemCount+=1;
    Amount=itemcost;
    TotalAmount+=Amount

    var cart=document.getElementById("carttable");
    cart.innerHTML+=
    `
    <tr>
        <th>${itemname}</th>
        <th>${itemcost}</th>
    </tr>
    `
    var CartTotal=document.getElementById("carttotal");
    CartTotal.innerHTML=
    `
    <tr>
        <th><b>TOTAL</b></th>
        <th>${TotalAmount}</th>
    </tr>
    `

}